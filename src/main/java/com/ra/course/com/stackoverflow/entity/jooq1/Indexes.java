/*
 * This file is generated by jOOQ.
 */
package com.ra.course.com.stackoverflow.entity.jooq1;


import com.ra.course.com.stackoverflow.entity.jooq1.tables.Account;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Answer;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Bounty;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Comment;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Member;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Notification;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Photo;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Question;
import com.ra.course.com.stackoverflow.entity.jooq1.tables.Tag;

import javax.annotation.processing.Generated;

import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code>public</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index ACCOUNT_PKEY = Indexes0.ACCOUNT_PKEY;
    public static final Index ANSWER_PKEY = Indexes0.ANSWER_PKEY;
    public static final Index BOUNTY_PKEY = Indexes0.BOUNTY_PKEY;
    public static final Index COMMENT_PKEY = Indexes0.COMMENT_PKEY;
    public static final Index MEMBER_PKEY = Indexes0.MEMBER_PKEY;
    public static final Index NOTIFICATION_PKEY = Indexes0.NOTIFICATION_PKEY;
    public static final Index PHOTO_PKEY = Indexes0.PHOTO_PKEY;
    public static final Index QUESTION_PKEY = Indexes0.QUESTION_PKEY;
    public static final Index TAG_PKEY = Indexes0.TAG_PKEY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index ACCOUNT_PKEY = Internal.createIndex("account_pkey", Account.ACCOUNT, new OrderField[] { Account.ACCOUNT.ID }, true);
        public static Index ANSWER_PKEY = Internal.createIndex("answer_pkey", Answer.ANSWER, new OrderField[] { Answer.ANSWER.ID }, true);
        public static Index BOUNTY_PKEY = Internal.createIndex("bounty_pkey", Bounty.BOUNTY, new OrderField[] { Bounty.BOUNTY.ID }, true);
        public static Index COMMENT_PKEY = Internal.createIndex("comment_pkey", Comment.COMMENT, new OrderField[] { Comment.COMMENT.ID }, true);
        public static Index MEMBER_PKEY = Internal.createIndex("member_pkey", Member.MEMBER, new OrderField[] { Member.MEMBER.ID }, true);
        public static Index NOTIFICATION_PKEY = Internal.createIndex("notification_pkey", Notification.NOTIFICATION, new OrderField[] { Notification.NOTIFICATION.ID }, true);
        public static Index PHOTO_PKEY = Internal.createIndex("photo_pkey", Photo.PHOTO, new OrderField[] { Photo.PHOTO.ID }, true);
        public static Index QUESTION_PKEY = Internal.createIndex("question_pkey", Question.QUESTION, new OrderField[] { Question.QUESTION.ID }, true);
        public static Index TAG_PKEY = Internal.createIndex("tag_pkey", Tag.TAG, new OrderField[] { Tag.TAG.ID }, true);
    }
}
